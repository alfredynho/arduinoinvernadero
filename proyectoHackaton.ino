#include <dht.h>
dht DHT;
#define DHT11_PIN 8

const int foco = 13;
const int ventilador = 12;

const int pin = 4;

const int PinSensorHumedad=0;
int lectura; 


//Amarrillo -> Tierra//
//# Rojo -> Foco//
//# Morado -> Ventilador//

void setup() 
{
   Serial.begin(9600);
   pinMode(foco, OUTPUT);
   pinMode(ventilador, OUTPUT);   
}
 
void loop()
{

   if (Serial.available()>0) 
   {
      char option = Serial.read();
      if (option == '0')
      {
        digitalWrite(foco, HIGH);
      }

      if(option == '1'){
        digitalWrite(foco,LOW);
      }

      if(option == '2'){
        digitalWrite(ventilador,HIGH);
      }

      if (option == '3')
      {
        digitalWrite(ventilador, LOW);   
      }   

     if (option == '4')
     {
        DHT.read11(DHT11_PIN);
    
        //TEMPERATURA//
        Serial.print("Temperatura = ");
        Serial.print(DHT.temperature);
        Serial.println(" C");
    
     }

     if (option == '5')
     {
        DHT.read11(DHT11_PIN);
    
        //HUMEDAD RELATIVA//
        Serial.print("Humedad = ");
        Serial.print(DHT.humidity);
        Serial.println(" %");
     }

     if(option == '6')
     {
        lectura = analogRead(PinSensorHumedad);
        Serial.print("Sensor de Humedad lectura: ");  
        Serial.print(lectura); 
        if (lectura <= 300) 
            {
            Serial.println(" SUELO SECO"); 
            }
        if ((lectura > 300) and (lectura <= 700))
            { 
          Serial.println(" SUELO ESTA HUMEDO"); 
            }
        if (lectura > 700)
          { 
          Serial.println(" SUELO MOJADO"); 
          }
     }

     if (option == '7')
     {
        digitalWrite(pin, HIGH);   // poner el Pin en HIGH
        Serial.println(" Comenzando Riego ..."); 

        delay(9000);               // esperar 10 segundos
        digitalWrite(pin, LOW);    // poner el Pin en LOW
        delay(9000);   
     }

   }
}
